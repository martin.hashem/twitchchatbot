import random
import math


def rollDice(sides):
    roll = math.floor(random.random()*sides) + 1
    return roll

def toss(): # redoing this into actually playing with the bot (like.... tossing her into the air :3 )
  if rollDice(2) == 1:
    return "yay"
  else:
    return "nay" 

def stringifyArray(arr):
  ret = "[ " + str(arr[0])
  for x in arr[1:]:
    ret = ret + ", " + str(x)
  ret = ret + " ]"
  return ret