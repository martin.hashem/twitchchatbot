import json
import re
import random
import time
import helpers
import requests
import constants
import maze

## REDO this to just be an handling messages type (even called by heartbeat)

### Let me google that for you link generation for every quostion asked xD

### Introductions

### command asking


def regSearch(term, cont):
  return re.search(term, cont, re.IGNORECASE)

def improveMood():
  while 1:
    url = constants.messageAPI % constants.users["Ichus"]["dmChannel"]
    res = [
      "I hope you are doing good :heart:",
      ":heart:",
      ]
    intv = random.randrange(600,3600)
    d = {"content": random.choice(res), "tts":False}
    print("random messages, next coming ", intv)
    requests.post(url=url, data=json.dumps(d), headers=constants.header)
    time.sleep(intv)


def debugMessage(res):
  url = constants.messageAPI % constants.users["Ichus"]["dmChannel"]
  d = {"content": res, "tts":False}
  print("debug messages")
  requests.post(url=url, data=json.dumps(d), headers=constants.header)

def ownerMessages(content, channel):
  url = constants.messageAPI % constants.users["Ichus"]["dmChannel"]
  res = ""
  if regSearch("you are my favourite",content) or regSearch("you are my favorite",content):
    res = "I :heart: you"
  elif regSearch("!toss", content):
    res = "WHEEE :heart:"
  d = {"content": res, "tts":False}
  requests.post(url=url, data=json.dumps(d), headers=constants.header)

def sortMessages(author, content, channel):
  print("sortMessage")
  res = ""
  url = constants.messageAPI % channel
  if author["id"] == constants.users["self"]["id"]:
    return
  elif author["id"] == constants.users["Ichus"]["id"]:
    ownerMessages(content,channel)
  if regSearch("!cointoss", content):
    res = helpers.toss()
  elif regSearch("!roll", content):
    
    args = content.split()
    sums = 0
    res = ""
    count = {"1":0,"<=10":0,">10":0,"20":0}
    rolls = []
    if len(args) == 1:
      roll = helpers.rollDice(100)
      rolls.append(roll)
      sums = roll
    elif re.search("d", args[1]):
      if re.search("^d",args[1]):
        args2 = [1, int(args[1][1:])]
      else:
        args2 = [int(x) for x in args[1].split("d")]
      while int(args2[0]) > 0:
        roll = helpers.rollDice(args2[1])
        if str(roll) in count:
          count[str(roll)] = count[str(roll)] + 1
        if roll <= 10:
          count["<=10"] = count["<=10"] + 1
        else:
          count[">10"] = count[">10"] + 1
        rolls.append(roll)
        sums = sums + roll
        args2[0] = args2[0]- 1
    else:
      roll = helpers.rollDice(int(args[1]))
      rolls.append(roll)
      sums = roll
    res = "**" + str(sums) + "** *" + helpers.stringifyArray(rolls) + "* " + json.dumps(count) 
  elif regSearch("\u2764", content):
    res = ":heart:"
    url = constants.messageAPI % constants.users["Ichus"]["dmChannel"]
  elif regSearch("!maze", content):
    maze.run(author)
    return
  else:
    return
  return url, {"content": res, "tts" : False}

def sendMessages(ws, recvMsg):
  
  if recvMsg["d"]["author"]["id"] == constants.users["self"]["id"]:
    
    return
  elif regSearch("!maze", recvMsg["d"]["content"]):
    
    maze.init(recvMsg["d"]["author"],recvMsg["d"]["guild_id"])
  #elif recvMsg["d"]["channel_id"] == constants.gameChannel[recvMsg["d"]["guild_id"]]:
  #  
  #  maze.playerActions(recvMsg["d"]["content"])

  url, d = sortMessages(recvMsg["d"]["author"], recvMsg["d"]["content"], recvMsg["d"]["channel_id"])
  
  requests.post(url=url, data=json.dumps(d), headers=constants.header)
