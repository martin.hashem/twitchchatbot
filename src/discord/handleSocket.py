import websocket
import asyncio
import ssl
import zlib
import json
import time
import threading
import requests
import messaging
import heartbeat
import main
import constants

lastMessage = {}
sector = 0
session_id = ""

def onMessage(ws, msg):
    global session_id
    lMsg = json.loads(msg)
#    print(json.dumps(lMsg, indent=2))
    if lMsg["op"] == 11:
        print("## heartbeat ACK ##")
    elif lMsg["op"] == 10:
        if session_id != "":
            sendIndentify(ws)
        else:
            heartbeat.beatIntervall = lMsg["d"]["heartbeat_interval"]
            sendIndentify(ws)
    elif lMsg["op"] == 1:
        heartbeat.sector = lMsg["d"]
    elif lMsg["op"] == 0:
        if lMsg["t"] == "MESSAGE_CREATE":
            print("what ever?")
#            messaging.sendMessages(ws, lMsg)
    global sector
    sector = lMsg["s"]
    print(sector)
    heartbeat.sequenz = int(sector)
    if lMsg["t"] == "READY":
        session_id = lMsg["d"]["session_id"]
    global lastMessage
    lastMessage = lMsg


def onError(ws, err):
    print("ERROR",err)
    raise err


def onClose(ws):
    print("## Connection closed ##")
    messaging.debugMessage("WebSocketApp Closed")
    exit(115)


def socketInit(uri):
    ws = websocket.WebSocketApp(
        uri, on_message=onMessage, on_error=onError, on_close=onClose
    )
    return ws

def sendIndentify(ws):
  d = {
    "op": 2,
    "d": {
        "token": constants.securityToken.token,
        "properties": {"$os": "linux", "$device": "MyBot", "$browser": "MyBrowser"},
    },
  }
  d = json.dumps(d)
  d = bytearray(d, encoding="utf-8")
  ws.send(d)

def resume(ws):
    d = {
        "op":6,
        "d":{
            "token":constants.securityToken.token,
            "session_id":session_id,
            "seq":sector
        }
    }
    b = bytearray(d, encoding="utf-8")
    ws.send(b)
