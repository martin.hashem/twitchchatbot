import threading
import handleSocket
import heartbeat
import messaging
import requests
import constants
import json
import time

threads = {}
ws = None

if __name__ == "__main__":
    r = requests.get("https://discordapp.com/api/gateway/bot",headers=constants.header)
    url = r.json()
    ws = handleSocket.socketInit(url["url"])
    threads["socket"] = threading.Thread(target=ws.run_forever, args=())    
    threads["heartbeat"] = threading.Thread(
        target=heartbeat.handleHeartbeat, args=(ws,)
    )
    moods = threading.Thread(target=messaging.improveMood, args=())
    for t in threads:
    #   messaging.debugMessage("starting thread "+t)
       threads[t].start()
    moods.start()
            
    while 1:
        if "socket" in threads and not threads["socket"].is_alive():
    #        messaging.debugMessage("joining treads")
            threads["socket"].join()
            print("joining treads heartbeat")
            threads["heartbeat"].join()
    #       messaging.debugMessage("rebuilding")
            r = requests.get("https://discordapp.com/api/gateway/bot",headers=constants.header)
            url = r.json()
            ws = handleSocket.socketInit(url["url"])
            threads["socket"] = threading.Thread(target=ws.run_forever, args=())    
            threads["heartbeat"] = threading.Thread(
                target=heartbeat.handleHeartbeat, args=(ws,)
            )
            for t in threads:
    #            messaging.debugMessage("starting thread "+t)
                threads[t].start()
        else:
          #  print("pass", threads["socket"].is_alive(), threads["heartbeat"].is_alive())
            time.sleep(5)