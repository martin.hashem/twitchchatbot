import time
import constants
import main
import messaging

beatIntervall = -1  # in Milliseconds
sequenz = "null"

def handleHeartbeat(ws):
    global beatIntervall
    while 1:
        time.sleep(5)
        if beatIntervall < 0:
            pass
        else:
            print(sequenz,type(sequenz),beatIntervall)
            if sequenz == "null":
                b = bytearray('{"op": 1,"d":%s}' % sequenz, encoding="utf-8")
            else:
                b = bytearray('{"op": 1,"d":%d}' % sequenz, encoding="utf-8")
            try:
                ws.send(b)
            except Exception as e:
                messaging.debugMessage("sending heartbeat failed")
                exit(110)
            time.sleep(2)
            time.sleep(beatIntervall / 1000 - 5 - 2)

